{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {
    self,
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
    in {
      packages = let
        # just need an empty fonts.conf
        fontConf = pkgs.makeFontsConf {fontDirectories = [];};
        makeSheetMusic = name:
          pkgs.stdenvNoCC.mkDerivation {
            inherit name;

            src = self;

            nativeBuildInputs = with pkgs; [
              lilypond-with-fonts
            ];

            buildPhase = ''
              export FONTCONFIG_FILE=${fontConf}
              lilypond ${name}.ly
            '';

            installPhase = ''
              mkdir $out
              cp ${name}.pdf $out/${name}.pdf
              cp ${name}.midi $out/${name}.midi
            '';
          };
      in {
        indigo-night = makeSheetMusic "indigo-night";
      };

      devShell = pkgs.mkShellNoCC {
        buildInputs = [
          pkgs.lilypond-with-fonts
          pkgs.wildmidi
        ];
      };
    });
}

\version "2.22.2"
\language "english"

\header {
  title = "Indigo Night"
  subtitle = "Tamino"
  pdftitle = "Indigo Night by Tamino"
}

introMelody = \relative {
  \numericTimeSignature
  \key ds \minor
  \time 4/4
  as'2\3 as\3 |
  \repeat volta 2 {
    b8\3 gs\3 as\3 fs\3 gs4 r |
    f4. ds8 f b\3 as\3 gs |
    fs\4 f ds2. |
    d4. ds8\glissando f4 as,8 r |
    \appoggiatura as2( b2.) \appoggiatura gs4 \glissando as4 |
    gs4. d'8 \glissando cs b cs b |
  }
  \alternative {
    { as2 as'2\3 | }
    { as,1 | }
  }
}

intro = \new StaffGroup <<
  \new Staff {
    \tempo 4 = 112
    \new Voice {
      \introMelody
    }
  }
  \new TabStaff {
    \clef moderntab
    \set TabStaff.minimumFret = #11
    \set TabStaff.restrainOpenStrings = ##t
    \introMelody
  }
>>

\score {
  \header {
    piece = "Intro"
  }

  \intro

  \layout {
    \context {
      \Score
      \override Glissando.minimum-length = #4
      \override Glissando.springs-and-rods = #ly:spanner::set-spacing-rods
      \override Glissando.thickness = #2
    }
    \omit Voice.StringNumber
  }
}

\score {
  \unfoldRepeats {
    \intro
  }

  \midi{}
}
